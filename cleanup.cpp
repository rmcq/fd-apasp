#include "apasp_impl.h"

#ifndef STATIC_DEBUG_DISABLED
extern bool debug;
#endif

triple apasp_impl::erase_edge(nodeid_t x, nodeid_t y)
{
	triple t(x, y, x, y, edge_weight(x, y), 1);

	if (inP(t))
		clean_triple(t, nullptr, nullptr);
	
	if (inPstar(t)) {
		mark_halves(t);
		clean_shortest_triple(t);
	}

	return t;
}

void apasp_impl::cleanup(nodeid_t v) {
	clear_cleanup_data();

	if (debug) cout <<"starting cleanup on " <<v <<endl;

	triple_heap hc;

	//Push all edges (v, x)
	for (graph_t::OutArcIt oi(*graph, graph->nodeFromId(v)); oi != lemon::INVALID; ++oi) {
		triple t = erase_edge(v, graph->id(graph->target(oi)));

		left_dist(t) = 0;
		right_dist(t) = t.weight;

		history(t).add(std::max(vtx_update_num[t.x], vtx_update_num[t.y]), 1);

		hc.push(t); //(v x, v x)
	}

	//Push all edges (x, v)
	for (graph_t::InArcIt ii(*graph, graph->nodeFromId(v)); ii != lemon::INVALID; ++ii) {
		triple t = erase_edge(graph->id(graph->source(ii)), v);

		left_dist(t) = t.weight;
		right_dist(t) = 0;

		history(t).add(std::max(vtx_update_num[t.x], vtx_update_num[t.y]), 1);

		hc.push(t); //(x v, x v)
	}

	std::vector<triple> S;

	while (hc.size()) {

		//Extract into S all the min-key triples from Hc
		S.clear();
		triple minkt = hc.top();
		nodeid_t x = minkt.x, y = minkt.y;
		S.push_back(minkt);
		hc.pop();
		extract_all_min_key(minkt, hc, S);

		std::vector<bool> seen(nnodes, false);

		//For each unique b of the triples in S
		for (const triple &st : S) {
			nodeid_t b = st.b;
			if (seen[b])
				continue;
			else seen[b] = true;

			//Iterate through S again, and find the minimum update-num 
			ui4 fcount = 0;	
			updatenum_counts sunc;
			for (const triple &st : S) {
				if (st.b != b)
					continue;
				fcount += st.count;
				for (auto &unc : history(st).uncs) {
					sunc.add(unc.update_num, unc.count);
				}
			}

			//For each extension in L(x, b, y)
			for (auto it = begin(L(x, b, y)); it != end(L(x, b, y));) {
				auto top_it = it;				
				nodeid_t xp = it->node;
				triple t(xp, x, b, y, minkt.weight + edge_weight(xp, x), fcount);

				if (xp == v) {
					++it;
					continue;
				}

				ui4 ldist = left_dist(st) + edge_weight(xp, x), rdist = right_dist(st);

				//Match weight of extension
				if (t.weight == it->weight) {

					//have we deleted anything of the form (x'x, _b) from P*(x', b)?
					if (v == y || (0 != marked_count(xp, x, 1+nnodes, b, t.weight - edge_weight(b, y)) && 0 != marked_count(x, 1+nnodes, b, y, t.weight - edge_weight(xp, x)))) {
						left_dist(t) = ldist;
						right_dist(t) = rdist;

						ui4 left_deleted_count = marked_count(xp, x, 1+nnodes, v, ldist), right_deleted_count = marked_count(v, 1+nnodes, b, y, rdist);
						ui4 ncc = left_deleted_count * right_deleted_count;
						if (ncc != t.count && v != y)
							t.count = ncc;

						ui4 id_xp = vtx_update_num[xp], psun = pstar_update_num(t), remove_P = 0, remove_Pstar = 0;
						updatenum_counts &nunc = history(t);
						nunc.clear();
						for (auto &unc : sunc.uncs) {
							if (unc.update_num <= psun)
								remove_Pstar += unc.count;
							nunc.add(std::max(id_xp, unc.update_num), unc.count);
							remove_P += unc.count;
						}

						process_cleanup(v, t, remove_Pstar, &it, nullptr, hc);

					}
				}
				if (it == top_it)
					++it;
			}
		}

		seen.assign(seen.size(), false);

		for (const triple &st : S) {
			nodeid_t a = st.a;
			if (seen[a])
				continue;
			else seen[a] = true;

			ui4 fcount = 0;
			updatenum_counts sunc;
			for (const triple &st : S) {
				if (st.a != a)
					continue;
				fcount += st.count;
				for (auto &unc : history(st).uncs) {
					sunc.add(unc.update_num, unc.count);
				}
			}

			for (auto it = begin(R(x, a, y)); it != end(R(x, a, y));) {
				auto top_it = it;
				nodeid_t yp = it->node;
				triple t(x, a, y, yp, minkt.weight + edge_weight(y, yp), fcount);

				if (yp == v) {
					++it;
					continue;
				}

				ui4 rdist = right_dist(st) + edge_weight(y, yp), ldist = left_dist(st);

				if (t.weight == it->weight) {
					if (v == x || (0 != marked_count(x, a, 1+nnodes, y, t.weight - edge_weight(y, yp)) && 0 != marked_count(a, 1+nnodes, y, yp, t.weight - edge_weight(x, a)))) {
						left_dist(t) = ldist;
						right_dist(t) = rdist;

						ui4 left_deleted_count = marked_count(v, 1+nnodes, y, yp, rdist), right_deleted_count = marked_count(x, a, 1+nnodes, v, ldist);
						ui4 ncc = left_deleted_count * right_deleted_count;
						if (ncc != t.count && v != x)
							t.count = ncc;

						ui4 id_yp = vtx_update_num[yp], psun = pstar_update_num(t), remove_P = 0, remove_Pstar = 0;
						updatenum_counts &nunc = history(t);
						nunc.clear();
						for (auto &unc : sunc.uncs) {
							if (unc.update_num <= psun)
								remove_Pstar += unc.count;
							nunc.add(std::max(id_yp, unc.update_num), unc.count);
							remove_P += unc.count;
						}
						process_cleanup(v, t, remove_Pstar, nullptr, &it, hc);
					}
				}
				if (it == top_it)
					++it;
			}
		}
	}
}

void apasp_impl::process_cleanup
(nodeid_t clv, triple &extd, ui4 n_delete_pstar, ext_set::iterator *lit, ext_set::iterator *rit, triple_heap &hc) {
	nodeid_t x = extd.x, y = extd.y;
	ui4 &mkct = marked_count(extd);
	if (mkct) {
		if (mkct < extd.count) {
			ui4 pcount = P(x, y).get_count(extd);
			if (0 == pcount)
				return;

			//set count of extd in P to (pcount - fcount + dcount)
			//decrement count of extd in P by (fcount - dcount)
			extd.count -= mkct;
			clean_triple(extd, lit, rit);
			clean_shortest_triple(extd);
		}
		return;
	}

	mkct = extd.count;

	ui4 existing_count = Pstar(x, y).get_count(extd);
	clean_triple(extd, lit, rit, n_delete_pstar < existing_count);

	hc.push(extd);

	if (existing_count) {

		if (existing_count < extd.count)
			extd.count = existing_count;

		//updatenum_counts 

		if (n_delete_pstar == 0)
			return;

		extd.count = n_delete_pstar;
		mark_halves(extd);

		if (extd.weight == sp_dist(x, y)) {
			clean_shortest_triple(extd); //ST
		} else {
			clean_shortest_triple(extd); //THT
		}
	}
}

bool apasp_impl::check_cleanup(nodeid_t v) {
	for (graph_t::NodeIt ni(*graph); ni != lemon::INVALID; ++ni) {
		nodeid_t x = graph->id(ni);

		if (!P(x, v).empty()) {
			cout <<"@@ P(" <<x <<", " <<v <<") = " <<P(x, v).str(x, v) <<" is not empty after cleanup on " <<v <<endl;
			return false;
		}
		if (!P(v, x).empty()) {
			cout <<"@@ P(" <<v <<", " <<x <<") = " <<P(v, x).str(v, x) <<" is not empty after cleanup on " <<v <<endl;
			return false;
		}

		for (graph_t::NodeIt nj(*graph); nj != lemon::INVALID; ++nj) {
			nodeid_t y = graph->id(nj);
			
			for (auto st : P(x, y))
				if (st.a == v || st.b == v) {
					cout <<"@@ triple " <<st.expand(x, y) <<" remains in P = " <<P(x, y).str(x, y) <<" after cleanup on " <<v <<endl;
					return false;
				}
			for (auto st : Pstar(x, y))
				if (st.a == v || st.b == v) {
					cout <<"@@ triple " <<st.expand(x, y) <<" remains in P* = " <<Pstar(x, y).str(x, y) <<" after cleanup on " <<v <<endl;
					return false;
				}
		}
	}
	return true;
}
