#include "nodata.h"
#include "print.h"

std::ostream& operator<<(std::ostream &s, const tuple &t) {
	return s <<"(" <<t.x <<" " <<t.a <<", " <<t.b <<" " <<t.y <<")";
}

std::ostream& operator<<(std::ostream &s, const triple &t) {
	return s << "(" <<static_cast<const tuple&>(t) <<", " <<t.weight <<", " <<t.count <<")";
}

std::ostream& operator<<(std::ostream &s, const std::vector<triple> &ts) {
	return put_set(s, begin(ts), end(ts));
}

std::ostream& operator<<(std::ostream &s, const boost::heap::d_ary_heap<triple,
						 boost::heap::arity<HEAP_ARITY>, boost::heap::mutable_<true> > &h) {
	return put_set(s, h.ordered_begin(), h.ordered_end());
}

std::ostream& operator<<(std::ostream &s, const weighted_extension &se) {
	return s << "<" <<se.weight <<", " <<se.node <<">";
}

std::ostream& operator<<(std::ostream &s, const std::unordered_set<nodeid_t> &ns) {
	return put_set(s, begin(ns), end(ns));
}

std::ostream& operator<<(std::ostream &s, const ext_set &ses) {
	return put_set(s, begin(ses), end(ses));
}
