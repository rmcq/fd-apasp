#pragma once

#include <cstdio>
#include <iostream>
#include <functional>
#include <numeric>
#include <vector>
#include <unordered_set>
#include <unordered_map>

#include "nodata.h"
#include "nograph.h"

#include "print.h"

using std::cout; using std::endl;

struct apasp_impl : public apasp_data {
	typedef boost::heap::d_ary_heap<triple, boost::heap::arity<HEAP_ARITY>, boost::heap::mutable_<true> > triple_heap;
	//typedef boost::heap::pairing_heap<triple> triple_heap;
	
	graph_t *graph;
	ui4 update_counter = 0;
	
	apasp_impl(graph_t *g, ui4 nnodes) : apasp_data(nnodes), graph(g) {}

	//see cleanup.cpp
	void cleanup(nodeid_t v);
	triple erase_edge(nodeid_t x, nodeid_t y);
	void process_cleanup(nodeid_t clv, triple &extd, ui4 n_delete_pstar, ext_set::iterator *lit, ext_set::iterator *rit, triple_heap &hc);

	//see fixup.cpp    
	void fixup(nodeid_t v, bool is_init=false);
	triple add_edge(nodeid_t x, nodeid_t y, nodeid_t v);
    void set_pstar_update_num(triple &t);
	void process_extension(const triple& extd, nodeid_t v, ui4 npaths, triple_heap &hf);
	
	ui4 get_fcount(const std::vector<triple> &S, std::function<bool(const triple&)> f) {
		return std::accumulate(begin(S), end(S), 0, [f](ui4 acc, const triple &t) {
				return f(t) ? acc + t.count : acc;
			});
	}

	bool check_cleanup(nodeid_t v);

	//vec already contains top, and the heap has already been popped once
	void extract_all_min_key(const triple &top, triple_heap &h, std::vector<triple> &vec) {
		while (!h.empty() && h.top().weight == top.weight && h.top().x == top.x && h.top().y == top.y) {
			//pop duplicates but don't add them
			if (h.top().a != vec.back().a || h.top().b != vec.back().b)
				vec.push_back(h.top());
			h.pop();
		}
	}
};
