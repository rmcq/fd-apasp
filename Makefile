CFLAGS  = -Wall -std=c++14 -Ofast -flto
COMPILER = clang++ #PCH stuff does not work as is with gcc
COMPILE  = $(COMPILER) $(CFLAGS)
MAKE_PCH = $(COMPILE) -x c++-header 

all: no

boost_include.h.pch: boost_include.h
	$(MAKE_PCH) boost_include.h -o boost_include.h.pch

SOURCES =  cleanup.cpp  fail.cpp  fixup.cpp  no.cpp  nograph.cpp  print.cpp
HEADERS = apasp_impl.h  apasp_driver.h fail.h  nodata.h  nograph.h  nots.h  notypes.h  print.h  tupletriple.h

nograph.o: nograph.h nograph.cpp notypes.h
	$(COMPILE) -c nograph.cpp

fail.o: fail.h fail.cpp
	$(COMPILE) -c fail.cpp

fixup.o: fixup.cpp apasp_impl.h nodata.h nots.h print.h print.cpp
	$(COMPILE) -include boost_include.h -c fixup.cpp

cleanup.o: cleanup.cpp apasp_impl.h nodata.h nots.h
	$(COMPILE) -include boost_include.h -c cleanup.cpp

no.o: no.cpp print.cpp apasp_driver.h
	$(COMPILE) -include boost_include.h -c no.cpp print.cpp 

no: boost_include.h.pch no.o nograph.o fail.o fixup.o cleanup.o
	$(COMPILE) -include boost_include.h no.o print.o cleanup.o fixup.o fail.o nograph.o -o no
