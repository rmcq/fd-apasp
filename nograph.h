#pragma once

#include <cstdio>
#include <vector>

#include "notypes.h"

/*
using boost::edge_weight_t;

typedef boost::adjacency_list <boost::vecS, boost::vecS, boost::bidirectionalS, boost::no_property,
	boost::property <edge_weight_t, weight_t> > graph_t;
*/

#include <lemon/list_graph.h>

typedef lemon::ListDigraph graph_t;

graph_t::ArcMap<weight_t> *read_graph(const char *ifn, graph_t &g);


	
