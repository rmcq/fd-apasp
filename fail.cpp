#include "fail.h"

void fail(const char *fmt, ...) {
	va_list args;
	va_start(args, fmt);
	vfprintf(stdout, fmt, args);
    //exit(17);
	throw 1;
	//exit(1);
}
