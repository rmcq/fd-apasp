#include <ctime>
#include <cstdlib>
#include <string>
#include <cstring>
#include <iostream>
#include <fstream>
#include <tuple>
#include <sstream>
#include <vector>
#include <map>

#include "notypes.h"
#include "tupletriple.h"
#include "nodata.h"
#include "fail.h"

#include "apasp_impl.h"
#include "apasp_driver.h"

void check(apasp_driver &d) {
	if (d.check())
		cout <<"check OK" <<endl;
	else {
		cout <<"check FAIL" <<endl;
        d.write_dot("fixup-error.dot");
		exit(1);
	}
}

void write_graph(std::string fn, graph_t &g, graph_t::ArcMap<weight_t> &ws) {
	FILE *out = fopen(fn.c_str(), "w");
	fprintf(out, "%d\n", lemon::countNodes(g));
	for (graph_t::ArcIt a(g); a != lemon::INVALID; ++a)
		fprintf(out, "%d %d %d\n", g.id(g.source(a)), g.id(g.target(a)), ws[a]);
	printf("wrote %s\n", fn.c_str());
	fclose(out);
}

void write_update_sequence(std::string fn, apasp_driver &d, std::size_t index) {
	FILE *out = fopen(fn.c_str(), "w");
	graph_t &g = *(d.real_graph);
	for (std::size_t i = index; i < d.past_updates.size(); i++) {
		auto arc = d.past_updates[i].arc;
		fprintf(out, ">%d %d %d\n", g.id(g.source(arc)), g.id(g.target(arc)), d.past_updates[i].old_weight);
	}
	printf("wrote %s\n", fn.c_str());
	fclose(out);
}

//attempt to reproduce the error; return TRUE if it is observed, FALSE if the updates run correctly
bool retry_error(apasp_driver &choked, long start, long stop) {
	graph_t::ArcMap<weight_t> ws(*choked.real_graph);
	choked.recover_weights(start, ws);
	apasp_driver d(choked.real_graph, &ws);
	d.initialize();

	for (long i = start; i < stop; i++)
		if (!d.do_update(choked.past_updates[i].arc, choked.past_updates[i].old_weight))
			return true;
	return false;
}

void backtrack_error(apasp_driver &choked, std::string tag) {
	using namespace std::string_literals;

	long stop = choked.past_updates.size(), start = stop - 1;
	const int backtrack_limit = 100;

	//fprintf(stderr, "backtracking to update %zu...\n", start);
	while (start >= 0) {
		if (stop - start > backtrack_limit) {
 			printf("reached backtrack limit %d (stop = %ld)\n", backtrack_limit, stop);
			exit(0);
		}
		if (retry_error(choked, start, stop)) {			
			graph_t::ArcMap<weight_t> orig_ws(*choked.real_graph);
			choked.recover_weights(start, orig_ws);
			write_graph("error-graph-"s + tag, *choked.real_graph, orig_ws);
			write_update_sequence("error-updates-"s + tag, choked, start);
			exit(1);
		}
		
		--start;
	}
}

#ifndef STATIC_DEBUG_DISABLED
bool debug = false;
#endif

int main(int argc, char *argv[]) {
	if (argc < 2)
		fail("%s: no filename\n", argv[0]);

	char *input_file = nullptr, *update_file = nullptr;
	if (!strcmp(argv[1], "debug")) {

		#ifndef STATIC_DEBUG_DISABLED
		debug = true;
		#endif

		input_file = argv[2];
		if (argc > 3)
			update_file = argv[3];
	} else {
		input_file = argv[1];
		//if (argc > 2)
		//	update_file = argv[2];
	}

	//cout <<__TIME__"reading from " <<input_file <<endl;

	graph_t g;
	graph_t::ArcMap<weight_t> *ws = read_graph(input_file, g);
	apasp_driver d(&g, ws);
	d.initialize();
	//check(d);

	srand(5);

	if (update_file) {
		if (debug) d.write_dot("pre-updates.dot");
		FILE *upds = fopen(update_file, "r");
		int nupd = 0;
		while (!feof(upds)) {
			nodeid_t u, v;
			weight_t w;
			fscanf(upds, ">%u %u %u\n", &u, &v, &w);
			if (!d.edge_valid(u, v)) {
				continue;
			}
			cout <<"\n\n#" <<++nupd <<" DECIDED TO update edge (" <<u <<", " <<v <<") to " <<w <<endl; 
			ui4 old_w = d.get_weight(u, v);
			bool ok = d.do_update(u, v, w);

			if (debug) d.write_dot("post-update-"+std::to_string(nupd)+".dot", " ("+std::to_string(old_w)+")", (d.get_weight(u, v) == old_w) ? "blue" : "red");
			if (!ok)
				exit(1);
			else cout <<"check OK" <<endl;
		}
		if (debug) cout <<"completed " <<nupd <<" updates" <<endl;
	} else {
		//auto seed = time(nullptr);
        auto seed = 0;
		for (graph_t::ArcIt a(g); a != lemon::INVALID; ++a)
            seed += (*ws)[a];

		//seed = 1478322452l;
		//std::cout <<"using seed " <<seed <<std::endl;
		srand(seed);

        //const int N = 10000;
		const int N = atoi(argv[2]);
		for (int i = 0; i < N; i++) {
            /*
            if (i % 1000 == 0)
            printf("%d\n", i);*/
			//fprintf(stderr, "#%d\t", i);
			if (!d.random_update(i)) {
				backtrack_error(d, std::string(input_file));
				fprintf(stderr, "could not recreate error\n");
				return 0;
			} else if (debug) cout <<"check OK" <<endl;

		}
		//fprintf(stdout, "%s: did %d updates\n", input_file, N);
	}


	return 0;
}
