#pragma once

#include <unordered_map>

template<class M>
struct map_adapter : public M {
	typedef M map_type;
	typedef typename M::key_type Key;
	typedef typename M::mapped_type Value;

	//M m;
	void set(const Key &k, Value v) {
		this->operator[](k) = v;
	}
};

