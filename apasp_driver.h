#pragma once

#include <math.h>
#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <memory>

#include <chrono>
//#include "../nanotime.h"


#include <lemon/dijkstra.h>

#include "nograph.h"
#include "apasp_impl.h"

using std::cout; using std::endl;
using namespace std::string_literals;

struct apasp_driver {
	struct past_update {
		graph_t::Arc arc;
		weight_t old_weight;
		past_update(graph_t::Arc a, weight_t w) : arc(a), old_weight(w) {}
	};

	graph_t *real_graph, *impl_graph;
	graph_t::ArcMap<weight_t> *real_weights;
	apasp_impl impl;
	std::vector<past_update> past_updates;

	apasp_driver(graph_t *g, graph_t::ArcMap<weight_t> *ws)
		: real_graph(g), impl_graph(new graph_t()), real_weights(ws),
		  impl(impl_graph, lemon::countNodes(*real_graph)) {}

	void initialize() {
		if (debug) cout <<"initialize " <<lemon::countNodes(*real_graph) <<endl;
		graph_t &rg = *real_graph, &ig = *impl_graph;
		
		for (nodeid_t n = 0; n < impl.nnodes; n++) {
			//if (n > 100) std::cerr <<"adding vertex " <<n <<endl;
			ig.addNode();

			graph_t::Arc arc;
			for (nodeid_t t = 0; t < n; t++) {
				if ((arc = lemon::findArc(rg, rg.nodeFromId(n), rg.nodeFromId(t))) != lemon::INVALID) {
					ig.addArc(ig.nodeFromId(n), ig.nodeFromId(t));
					impl.edge_weight(n, t) = (*real_weights)[arc];
				}

				if ((arc = lemon::findArc(rg, rg.nodeFromId(t), rg.nodeFromId(n))) != lemon::INVALID) {
					ig.addArc(ig.nodeFromId(t), ig.nodeFromId(n));
					impl.edge_weight(t, n) = (*real_weights)[arc];
				}
			}

			impl.fixup(n, true);
		}
	}

	ui4 get_weight(nodeid_t u, nodeid_t v) {
		graph_t::Arc arc = lemon::findArc(*real_graph, real_graph->nodeFromId(u), real_graph->nodeFromId(v));
		return (*real_weights)[arc];
	}

	bool edge_valid(nodeid_t u, nodeid_t v) {
		return lemon::findArc(*real_graph, real_graph->nodeFromId(u), real_graph->nodeFromId(v)) != lemon::INVALID;
	}

	bool do_update(nodeid_t u, nodeid_t v, weight_t w) {
		graph_t::Arc arc = lemon::findArc(*real_graph, real_graph->nodeFromId(u), real_graph->nodeFromId(v));
		if (arc == lemon::INVALID) {
			std::cerr <<"skipping invalid update (" <<u <<", " <<v <<") to " <<w <<std::endl;
			return true;
			//fail("could not find edge (%u, %u) in graph\n", u, v);
			//return false;
		} else return do_update(arc, w);
	}
	bool do_update(const graph_t::Arc &arc, weight_t w) {
		nodeid_t source = real_graph->id(real_graph->source(arc)), target = real_graph->id(real_graph->target(arc));
        if (debug) {
			printf("precleanup\n");
			print();
		}

		past_updates.emplace_back(arc, w);

		//unsigned long long ta = process_cputime();

		//auto ta = std::chrono::steady_clock::now();
		impl.cleanup(source);
		//auto tb = std::chrono::steady_clock::now();
		//std::size_t us_taken = std::chrono::duration_cast<std::chrono::microseconds>(tb-ta).count();
		//if (impl.update_counter > 256)
		//fprintf(stderr, "%lu\n", us_taken);

		if (!impl.check_cleanup(source))
			return false;

		if (debug) {
			printf("postcleanup\n");
			print("(postcleanup)");
		}

        //weight_t old_w = (*real_weights)[arc];
		(*real_weights)[arc] = w;
		impl.edge_weight(source, target) = w;

		//cout <<"decided to update " <<source <<", " <<target <<" to " <<w <<" (from " <<old_w <<")" <<endl;

		impl.fixup(source);

		//unsigned long long elapsed = process_cputime() - ta;

		//fprintf(stderr, "%f\n", ((double)elapsed)/(NANOSEC/1000.0));
		//fprintf(stdout, ">%u %u %u\n", source, target, w);

		return check();
	}

	bool random_update(ui4 nupd) {
		ui4 arcid = rand() % lemon::countArcs(*real_graph);
		graph_t::Arc arc = real_graph->arcFromId(arcid);
		float n = rand() / (float)RAND_MAX;
		int w = (*real_weights)[arc], old_w = w;
		if (rand() > RAND_MAX/2)
			w = ceil(w + (n/2)*w);
		else w = floor(w - (n/2)*w);

		if (w == 0)
			w = 1;

		bool r = do_update(arc, w);
        if (debug) write_dot("post-update-"+std::to_string(nupd)+".dot", " ("+std::to_string(old_w)+")");
        return r;
	}

	void recover_weights(std::size_t index, graph_t::ArcMap<weight_t> &ws) {
		for (graph_t::ArcIt a(*real_graph); a != lemon::INVALID; ++a)
			ws[a] = (*real_weights)[a];

		for (std::size_t i = 0; i < index; i++)
			ws[past_updates[i].arc] = past_updates[i].old_weight;

	}

	void print(const char *label = nullptr) {
		for (nodeid_t x = 0; x < impl.nnodes; x++)
			for (nodeid_t y = 0; y < impl.nnodes; y++) {
				if (impl.Pstar(x, y).size()) {
					if (label) cout <<label <<" ";
					cout <<"P*(" <<x <<", " <<y <<") := " <<impl.Pstar(x, y).expanded(x, y) <<endl;
					if (label) cout <<label <<" ";
					cout <<"P(" <<x <<", " <<y <<")  := " <<impl.P(x, y).str(x, y) <<endl;
					cout <<endl;
				}
			}
	}

	bool check() {
		bool any_wrong = false;
		for (nodeid_t n = 0; n < impl.nnodes; n++) {
			graph_t::NodeMap<weight_t> dist(*impl_graph);
			lemon::dijkstra(*real_graph, *real_weights).distMap(dist).run(real_graph->nodeFromId(n));

			for (nodeid_t v = 0; v < impl.nnodes; v++) {
				if (n == v)
					continue;
				weight_t correct_dist = dist[real_graph->nodeFromId(v)];
				if (correct_dist == 0) {
					if (impl.Pstar(n, v).size()) {
						cout <<"no paths from " <<n <<" to " <<v <<" but we have " <<"P*(" <<n <<", " <<v
							 <<")= " <<impl.Pstar(n, v).expanded(n, v) <<endl;
						any_wrong = true;
					}
				} else if (impl.Pstar(n, v).empty() || impl.Pstar(n, v).begin()->weight != correct_dist) {
					cout <<"FAILED: correct distance from " <<n <<" to " <<v <<" is " <<correct_dist <<" but we have "
						 <<"P*(" <<n <<", " <<v <<")= " <<impl.Pstar(n, v).expanded(n, v) <<endl;
					any_wrong = true;
				}
			}
		}
		return !any_wrong;
	}
    void write_dot(const std::string &fn, std::string extra=""s, std::string color="red"s) {
        FILE *out = fopen(fn.c_str(), "w");
        fprintf(out, "digraph G { \ngraph [ranksep=\"1\", nodesep=\"1\"];\nordering=in \n");
        for (graph_t::ArcIt ai(*real_graph); ai != lemon::INVALID; ++ai) {
            fprintf(out, "%d -> %d", real_graph->id(real_graph->source(ai)), real_graph->id(real_graph->target(ai)));
			if (past_updates.size() && ai == past_updates.back().arc)
				fprintf(out, " [label=\"%d%s\"] [color=%s penwidth=2]", (*real_weights)[ai], extra.c_str(), color.c_str());
			else
				fprintf(out, " [label=\"%d\"]", (*real_weights)[ai]);
            fprintf(out, "\n");
        }
        fprintf(out, "}\n");
        fclose(out);
		printf("wrote %s\n", fn.c_str());
    }

};
