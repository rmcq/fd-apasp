#!/bin/bash

# $1 = error-graph-XXXX
# $2 = error-upds-XXXX
# $3 = exe
# $4 = tag
minimize_error()
{
	NLINES=$(wc -l "$1" | cut -d ' ' -f 1)
	NEDGES=$[NLINES-1]
	echo "$1: $NEDGES edges"

	SED_CMD=""

	#start from 2 so we don't delete the number of nodes on the 1st line  
	for DEL_EDGE in $(seq 2 $NLINES); do
		NC=$SED_CMD$DEL_EDGE'd;'
		sed -e $NC "$1" >"/tmp/del_edge$4"
		
		"$3" "/tmp/del_edge$4" "$2" >/dev/null 2>/dev/null
		#if the error code is 1, the error was reproduced and the edge is nonessential
		if [ $? -eq 1 ]
		then
			SED_CMD=$NC
		fi
	done
	rm "/tmp/del_edge$4"

	if [ -z $SED_CMD ]
	then
		cp "$1" "$1.minimal"
	else
		echo "sed cmd = $SED_CMD"
		sed -e $SED_CMD "$1" >"$1.minimal"
	fi
}

double_minimize()
{
	minimize_error $1 $2 $3 $4
	minimize_error "$1.minimal" $2 $3 $4
	mv "$1.minimal.minimal" "$1.minimal"
}

#we have a bunch of files like error-graph-XXXX and error-updates-XXXX
#if we use `ls error-graph-*` we also count the .minimal files which is bad
#TAGS=$(ls error-updates-* | cut -d '-' -f 3)
#for XXXX in $TAGS; do

XXXX=$2

NDIR="report $XXXX"
mkdir -p "$NDIR"
cd "$NDIR"
rm -f *html

EXE="../$1"
GRAPH="../error-graph-$XXXX"
UPDATES="../error-updates-$XXXX"
if [ ! -f "$GRAPH.minimal" ]; then
	minimize_error $GRAPH $UPDATES $EXE $XXXX
#	double_minimize $GRAPH $UPDATES $EXE $XXXX
fi

echo "$GRAPH.minimal"
"$EXE" debug "$GRAPH.minimal" "$UPDATES" >out

for f in $(ls *dot); do
	dot -Kdot -Tsvg -o "$f.svg" "$f"
done

sed --in-place -e 's/#\([0-9]*\) DE/<\/pre><br><img src="post-update-\1.dot.svg"><br><pre>#\1 DE/g' out
echo "<html><head><meta charset='utf-8'><title>$XXXX</title></head><body>" >>"$XXXX.html"
echo '<img src="pre-updates.dot.svg"><br><pre>' >>"$XXXX.html"
cat out >>"$XXXX.html"
echo "</pre></body></html>" >>"$XXXX.html"
rm out

cd ..

#done


