#pragma once

#include <string>
#include <sstream>
#include <vector>
#include <unordered_map>

#include "notypes.h"
#include "tupletriple.h"
#include "fail.h"

//arity of Hc and Hf heaps, NOT the arity of P and P* (tripleset) heaps
#define HEAP_ARITY 4


struct tripleset {
	//since we already know x and y in order to access P(x, y) or P*(x, y), we only need store a, b, wt, and count
	struct stored_triple {
		nodeid_t a,b;
		weight_t weight;
		ui4 count;
		char beta;
		stored_triple(nodeid_t aa, nodeid_t bb, weight_t w, ui4 ct)
			: a(aa), b(bb), weight(w), count(ct), beta(0) {}
		stored_triple(const triple &t)
			: a(t.a), b(t.b), weight(t.weight), count(t.count), beta(0) {}

		bool operator==(const stored_triple &that) const {
			return weight == that.weight && a == that.a && b == that.b;
		}
		//compare by wt, beta to get the order we need in P and P*
		bool operator<(const stored_triple &that) const {
			if (weight == that.weight)
				return that.beta < beta;
			return that.weight < weight;
		}

		triple expand(nodeid_t x, nodeid_t y) const {
			return triple(x, a, b, y, weight, count);
		}		
	};

	//boost::heap::pairing_heap<stored_triple> heap;
	boost::heap::d_ary_heap<stored_triple, boost::heap::arity<2>, boost::heap::mutable_<true> > heap;
	std::unordered_map<triple, typename decltype(heap)::handle_type, triple::hash> handle_map;

	size_t size() const {
		if (heap.size() != handle_map.size())
			fail("unequal sizes\n");
		return heap.size();
	}

	std::string str(nodeid_t x, nodeid_t y) {
		if (empty())
			return "{}";

		std::stringstream out;
		auto it = begin();
		out <<"{((" <<x <<" " <<it->a <<", " <<it->b <<" " <<y <<"), " <<it->weight <<", " <<it->count <<" β=" <<(int)it->beta <<")";
		while (++it != end())
			out <<", ((" <<x <<" " <<it->a <<", " <<it->b <<" " <<y <<"), " <<it->weight <<", " <<it->count <<" β=" <<(int)it->beta <<")";
		out <<"}";
		return out.str();
	}

	typename decltype(heap)::ordered_iterator begin() { return heap.ordered_begin(); }
	typename decltype(heap)::ordered_iterator end()   { return heap.ordered_end();   }

	bool empty() const                   { return size() == 0; }
	bool contains(const triple &t) const { return handle_map.count(t) > 0; }

	ui4 get_count(const triple &t) {
		auto it = handle_map.find(t);
		if (it == handle_map.end())
			return 0;
			//fail("attempt to get count of triple %s which does not exist\n", t.cstr());
		return (*it->second).count;
	}

	//pass a,b as nodeids so we don't have to deal with triple vs. stored_triple
	bool has_triple_for_tuple(nodeid_t a, nodeid_t b) {
		//linear scan.....
		for (auto it = begin(); it != end(); ++it)
			if (it->a == a && it-> b == b)
				return true;
		return false;
	}

	ui4 count_with_wt(ui4 weight) {
		ui4 count_sum = 0;
		//linear scan again....
		for (auto it = begin(); it != end(); ++it)
			if (it->weight == weight)
				count_sum += it->count;
		return count_sum;
	}

	//we only need to check for b here, the check for y is
	//implied by the fact that we are looking in P*(x, y)
	ui4 count_ending_with(ui4 weight, ui4 b) {
		ui4 s = 0;
		for (auto it = begin(); it != end(); ++it)
			if (it->weight == weight && it->b == b)
				s += it->count;
		return s;
	}
	//likewise
	ui4 count_starting_with(ui4 weight, ui4 a) {
		ui4 s = 0;
		for (auto it = begin(); it != end(); ++it)
			if (it->weight == weight && it->a == a)
				s += it->count;
		return s;
	}
	
	void erase(const triple &t) {
		auto it = handle_map.find(t);
		if (it == handle_map.end())
			return;

		//can't remove directly; instead we can ensure it is on top and then pop
		(*(it->second)).beta = 0;
		(*(it->second)).weight = 0; //no operator->
		heap.increase(it->second);
		heap.pop();
		handle_map.erase(it);
	}

	//we don't need to do any checks when we are putting edges back in P
	//in add_edge at the start of fixup, since we just deleted them all in cleanup
	void push(const triple &t) {
		handle_map[t] = heap.push(t);
	}

	void add_triple(const triple &t) {
		auto it = handle_map.find(t);
		if (it == handle_map.end())
			handle_map[t] = heap.push(t);
		else modify_count(it, t.count);
	}
	
	//return true when it was already in the set
	bool add_triple(const triple &t, ui4 npaths) {
		auto it = handle_map.find(t);
		if (it == handle_map.end()) {
			handle_map[t] = heap.push(t);
			return false;
		} else if (npaths != 0)
			modify_count(it, npaths);
		else
			(*it->second).count = t.count;

		return true;
	}

	bool decrement_triple(const triple &t) {
		return modify_count(t, -t.count);
	}
	bool modify_count(decltype(handle_map)::iterator &it, int32_t delta) {
		stored_triple &stref = *it->second;
		int32_t new_count = stref.count + delta;

		if (new_count < 1) {
			stref.beta = 0;
			stref.weight = 0;
			heap.increase(it->second);
			heap.pop();
			handle_map.erase(it);
			return false;
		}
		stref.count = new_count;
		return true;
	}

	//return true if the triple still exists
	bool modify_count(const triple &t, int32_t delta) {
		auto it = handle_map.find(t);
		if (it == handle_map.end())
			return false;
		return modify_count(it, delta);
	}
	//you CANNOT set_count to 0
	void set_count(const triple &t, ui4 nc) {
		auto it = handle_map.find(t);
		if (it == handle_map.end())
			return;
		(*it->second).count = nc;
	}

	void set_beta(const triple &t, char beta) {
		auto it = handle_map.find(t);
		if (it == handle_map.end())
			return;

		(*it->second).beta = beta;
		heap.update(it->second);
	}

	std::vector<triple> expanded(nodeid_t x, nodeid_t y) {
		std::vector<triple> v;
		for (auto t : *this)
			v.push_back(t.expand(x, y));
		return v;
	}
};
