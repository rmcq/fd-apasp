#pragma once

#include <cstdio>
#include <cstdarg>
#include <cstdlib>

void fail(const char *fmt, ...);
