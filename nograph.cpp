#include "nograph.h"

graph_t::ArcMap<weight_t> *read_graph(const char *ifn, graph_t &g) {
	FILE *in = fopen(ifn, "r");
	ui4 nnodes;
	fscanf(in, "%d\n", &nnodes);

	graph_t::Node *ns = new graph_t::Node[nnodes];
	for (ui4 i = 0; i < nnodes; i++)
		ns[i] = g.addNode();
	graph_t::ArcMap<weight_t> *ws = new graph_t::ArcMap<weight_t>(g);

	while (!feof(in)) {
		nodeid_t u, v;
		weight_t w;
		fscanf(in, "%d %d %d\n", &u, &v, &w);

		if (w == 0)
			w = 1;

		graph_t::Arc a = g.addArc(ns[u], ns[v]);
		(*ws)[a] = w;
	}
	return ws;
}
