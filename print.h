#pragma once

#include <tuple>
#include <ostream>
#include <vector>

#include "tupletriple.h"
#include "nots.h"


std::ostream& operator<<(std::ostream &s, const tuple &t);
std::ostream& operator<<(std::ostream &s, const triple &t);

template<class T>
static std::ostream& put_set(std::ostream &s, T start, T finish) {
	if (start == finish)
		return s <<"{}";
	s <<"{" <<*start;
	while (++start != finish)
		s <<", "<<*start;
	return s <<"}";
}

std::ostream& operator<<(std::ostream &s, const std::vector<triple> &ts);
std::ostream& operator<<(std::ostream &s, const boost::heap::d_ary_heap<triple,
						 boost::heap::arity<HEAP_ARITY>, boost::heap::mutable_<true> > &h);

std::ostream& operator<<(std::ostream &s, const weighted_extension &se);

std::ostream& operator<<(std::ostream &s, const std::unordered_set<nodeid_t> &ns);
std::ostream& operator<<(std::ostream &s, const ext_set &ses);

