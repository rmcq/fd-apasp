#include "apasp_impl.h"

#ifndef STATIC_DEBUG_DISABLED
extern bool debug;
#endif

void apasp_impl::set_pstar_update_num(triple &t) {
	pstar_update_num(t) = update_counter;
	nodeid_t temp = t.a;
	t.a = 1 + nnodes;
	pstar_update_num(t) = update_counter;
	t.a = temp;
	temp = t.b;
	t.b = 1+nnodes;
	pstar_update_num(t) = update_counter;
	t.b = temp;
}

//check if x' or y' matches some a or b in S
//know which triple from S we are extending

struct uniq_data {
	ui4 fcount = 0, npaths = 0, maxpaths = 0;
	weight_t left_dist = 0, right_dist = 0;
};

void apasp_impl::fixup(nodeid_t v, bool is_init) {
	vtx_update_num[v] = update_counter;

	triple_heap hf;

	//For each pair of nodes (x, y), push the min-weight path in P(x, y)
	for (nodeid_t x = 0; x < nnodes; x++)
		for (nodeid_t y = 0; y < nnodes; y++) {
			mat(x, y).have_extracted = false;
			mat(x, y).clinfo.clear();
			if (!P(x, y).empty())
				hf.push(P(x,y).begin()->expand(x, y));
		}

	//Push all edges (v, x)
	for (graph_t::OutArcIt oi(*graph, graph->nodeFromId(v)); oi != lemon::INVALID; ++oi) {
		triple t = add_edge(v, graph->id(graph->target(oi)), v);
		left_dist(t) = 0;
		right_dist(t) = t.weight;
		hf.push(t); //(v x, v x)
	}

	//Push all edges (x, v)
	for (graph_t::InArcIt ii(*graph, graph->nodeFromId(v)); ii != lemon::INVALID; ++ii) {
		triple t = add_edge(graph->id(graph->source(ii)), v, v);
		left_dist(t) = t.weight;
		right_dist(t) = 0;
		hf.push(t); //(x v, x v)
	}

	std::vector<triple> Sp;
	while (hf.size()) {
		//Extract into S' all of the min-key triples from Hf
		Sp.clear();
		triple minkt = hf.top();
		nodeid_t x = minkt.x, y = minkt.y;
		Sp.push_back(minkt);
		hf.pop();
		extract_all_min_key(minkt, hf, Sp);

		if (have_extracted(x, y))
			continue;
		have_extracted(x, y) = true;

		weight_t old_dist = sp_dist(x, y);
		sp_dist(x, y) = minkt.weight;

		std::unordered_map<ui4, uniq_data> auniq, buniq;

		auto add_to_S = [&](const triple &t) {
							uniq_data &ad = auniq[t.a], &bd = buniq[t.b];

							ui4 vpaths = update_num(t) == update_counter ? paths(t, v) : 0;
							ad.fcount += t.count;
							ad.npaths += vpaths;

							bd.fcount += t.count;
							bd.npaths += vpaths;

							if (vpaths > ad.maxpaths) {
								ad.left_dist = left_dist(t);
								ad.right_dist = right_dist(t);
								ad.maxpaths = vpaths;
							}
							if (vpaths > bd.maxpaths) {
								bd.left_dist = left_dist(t);
								bd.right_dist = right_dist(t);
								bd.maxpaths = vpaths;
							}
						};

		//If P*(x, y) is empty or contains only historical triples
		if (is_init || (Pstar(x, y).empty() || Pstar(x, y).begin()->weight > old_dist)) {

			//Confirm any min-weight paths with beta=0 in P(x, y)
			if (P(x, y).size()) {
				//Can't change beta while iterating
				std::vector<triple> set_beta_to_1;
				ui4 minwt = P(x, y).begin()->weight;
				for (auto it = P(x, y).begin(); it != P(x, y).end() && it->weight == minwt; ++it) {
					triple t = it->expand(x, y);
					if (it->beta == 1)
						continue;

					if (!inPstar(t))
						add_shortest_triple(t, 0);
					else if (t.count != Pstar(x, y).get_count(t))
						Pstar(x, y).modify_count(t, t.count - Pstar(x, y).get_count(t));

					add_to_S(t);

					set_pstar_update_num(t);
					set_beta_to_1.push_back(t);
				}

				for (const triple &t : set_beta_to_1)
					set_beta(t, 1);
			}
		} else {
			//If there are any paths in S that we have created in this fixup, and that go through v, then we can confirm those too.
			for (triple &t : Sp)
				if (update_num(t) == update_counter && has_path_through(t, v)) {
					add_shortest_triple(t, paths(t, v));
					set_pstar_update_num(t);
					set_beta(t, 1);
					add_to_S(t);
				}
		}

		//If we have somehow managed to add no triples to S then we can just leave now
		if (auniq.empty())
			continue;

		//For each unique b of the triples in S
		for (auto ib = buniq.begin(); ib != buniq.end(); ++ib) {
			nodeid_t b = ib->first;
			ui4 fcount = ib->second.fcount;

			//For each extension in L*(x, b)
			for (auto ext : Lstar(x, b)) {
				nodeid_t xp = ext.node;
				if (xp == y)
					continue;

				//fcount is not the real count, but it will be important later and we can
				//do some less expensive checks before computing the real count
				triple extd(xp, x, b, y, minkt.weight + edge_weight(ext.node, x), fcount);

				//reject extensions with weights that don't match
				if (extd.weight - edge_weight(b, y) != ext.weight)
					continue;

				//If there is already a shorter path from x' to b in P*...
				if (Pstar(xp, b).empty() || Pstar(xp, b).begin()->weight < extd.weight - edge_weight(b, y)) {
					//...and the triple we are creating is already in P, then it must be true historical
					if (P(xp, y).contains(extd))
						continue;
				}

				//The count of the extended triple ((x' x, b y), wt) is equal to the sum of the counts
				//of all triples ((x' x, _ b), wt - w(b, y))
				ui4 rcount = Pstar(xp, b).count_starting_with(extd.weight - edge_weight(b, y), x);
				extd.count = rcount;

				if (extd.count < 1)
					continue;

				ui4 npaths = ib->second.npaths, maxpaths_ldist = ib->second.left_dist, maxpaths_rdist = ib->second.right_dist;

				if (v != y && npaths > 0 && pstar_update_num(triple(xp, x, 1+nnodes, b, extd.weight - edge_weight(b, y), 0)) != update_counter) {
					if (extd.count < fcount)
						npaths = 0;
					else continue;
				}

				//Calculate and store our l/rdist, and make sure that our ldist matches the weight in P*(x', v)
				ui4 ldist = maxpaths_ldist + edge_weight(xp, x);
				if (maxpaths_ldist > 0 && npaths > 0) {
					if (v != y && 0 == Pstar(xp, v).count_with_wt(ldist)) {
						continue;
					}
				}
				left_dist(extd) = ldist;
				right_dist(extd) = maxpaths_rdist;

				//update paths, set update_num, push to Hf, add to P, set beta to 0
				process_extension(extd, v, npaths, hf);
			}
		}

		for (auto ia = auniq.begin(); ia != auniq.end(); ++ia) {
			nodeid_t a = ia->first;
			ui4 fcount = ia->second.fcount;

			for (auto ext : Rstar(a, y)) {
				nodeid_t yp = ext.node;
				if (yp == x)
					continue;

				//fcount is not the real count, but we can do some less expensive checks before computing the real count
				triple extd(x, a, y, ext.node, minkt.weight + edge_weight(y, ext.node), fcount);

				//reject extensions with weights that don't match
				if (extd.weight - edge_weight(x, a) != ext.weight) {
					continue;
				}

				//reject extensions which do not match min weight in P*
				if (Pstar(a, yp).empty() || Pstar(a, yp).begin()->weight < extd.weight - edge_weight(x, a)) {
					//extd is not locally shortest 
					if (P(x, yp).contains(extd)) {
						continue;
					}
				}

				//Compute the real count
				extd.count = Pstar(a, yp).count_ending_with(extd.weight - edge_weight(x, a), y);

				if (extd.count < 1)
					continue;

				ui4 npaths = ia->second.npaths, maxpaths_ldist = ia->second.left_dist, maxpaths_rdist = ia->second.right_dist;

				if (v != x && npaths > 0 && pstar_update_num(triple(a, 1+nnodes, y, yp, extd.weight - edge_weight(x, a), 0)) != update_counter) {
					if (extd.count < fcount)
						npaths = 0;
					else continue;
				}

				//If this triple has paths through v, those subpaths must already be in P*.  Otherwise, we are probably creating a cycle.
				ui4 rdist = maxpaths_rdist + edge_weight(y, yp);
				if (maxpaths_rdist > 0 && npaths > 0) {
					if (v != x && 0 == Pstar(v, yp).count_with_wt(rdist)) {
						continue;
					}
				}
				left_dist(extd) = maxpaths_ldist;
				right_dist(extd) = rdist;

				process_extension(extd, v, npaths, hf);
			}
		}
	}
	++update_counter;
}

void apasp_impl::process_extension(const triple& extd, nodeid_t v, ui4 npaths, triple_heap &hf) {
	ui4 &mkct = marked_count(extd);
	if (mkct) {
		if (mkct < extd.count) {
			paths(extd, v) = npaths;
			P(extd.x, extd.y).set_count(extd, extd.count);
		}
		return;
	}
	mkct = extd.count;

	paths(extd, v) = npaths;
	update_num(extd) = update_counter;
	hf.push(extd);

	add_triple(extd, npaths);
	set_beta(extd, 0);
}

triple apasp_impl::add_edge(nodeid_t x, nodeid_t y, nodeid_t v) {
	triple t(x, y, x, y, edge_weight(x, y), 1);
	paths(t, v) = 1;
	update_num(t) = update_counter;
	P(t.x, t.y).push(t);
	return t;
}
