#pragma once

#include <cstdint>
#include <utility>

typedef uint32_t ui4;

typedef ui4 nodeid_t;
typedef ui4 weight_t;

//#define STATIC_DEBUG_DISABLED

#ifdef STATIC_DEBUG_DISABLED
constexpr bool debug = false;
#endif
