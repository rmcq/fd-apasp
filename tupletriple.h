#pragma once

#include <cstdio>
#include <string>
#include <sstream>

#include "notypes.h"

struct tuple {
	nodeid_t x, y, a, b;
	tuple(nodeid_t _x, nodeid_t _a, nodeid_t _b, nodeid_t _y) : x(_x), y(_y), a(_a), b(_b) {}

	bool operator==(const tuple& that) const {
		return x == that.x && y == that.y && a == that.a && b == that.b;
	}
	bool operator<(const tuple& that) const {
		//compare on x and y first so that we don't need a special comparator for Hc and Hf
		if (x == that.x) {
			if (y == that.y) {
				if (a == that.a)
					return that.b < b;
				return that.a < a;
			} else return that.y < y;
		} else return that.x < x;
	}

	std::string str() const {
		std::stringstream ss;
		ss <<"(" <<x <<" " <<a <<", " <<b <<" " <<y <<")";
		return ss.str();
	}
	std::size_t get_hash() const noexcept {
		return (x*a << 12) + b*y;
	}
	struct hash { std::size_t operator()(const tuple &t) const noexcept { return t.get_hash(); }};
};


static const size_t strbufsize = 64;
static char trip_str_buf[strbufsize];

//what you need to resolve a triple once you already know x, y
struct triple_abw {
	nodeid_t a, b;
	weight_t weight;
	explicit triple_abw(nodeid_t aa, nodeid_t bb, weight_t w) : a(aa), b(bb), weight(w) {}

	bool operator==(const triple_abw &that) const  {
		return that.weight == weight && that.a == a && that.b == b;
	}
	struct hash { std::size_t operator()(const triple_abw &t) const noexcept {
		return 0x4a3 * t.a + 0x1f * t.b + t.weight;
	}};
};


struct triple : public tuple {
	weight_t weight;
	mutable ui4 count; //not part of the hash
	triple(nodeid_t x, nodeid_t a, nodeid_t b, nodeid_t y, weight_t wt, size_t ct)
		: tuple(x,a,b,y), weight(wt), count(ct) {}

	operator const triple_abw&() const {
		return * reinterpret_cast<const triple_abw *>(&a);
	}

	//comparison methods compare on weight and tuple
	bool operator==(const triple &that) const {
		return weight == that.weight && tuple::operator==(static_cast<const tuple&>(that));
	}
	bool operator<(const triple &that) const {
		if (weight == that.weight)
			return tuple::operator<(static_cast<const tuple&>(that));
		return that.weight < weight;
	}
	struct hash { std::size_t operator()(const triple &t) const noexcept {
		return t.get_hash() * t.weight;
	}};

	char *cstr() const {
		snprintf(trip_str_buf, strbufsize, "((%u %u, %u %u), %u, %u)", x, a, b, y, weight, count);
		return trip_str_buf;
	}
};


