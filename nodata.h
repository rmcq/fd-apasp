#pragma once

#include <iostream>
#include <functional>
#include <unordered_map>
#include <unordered_set>

#include "notypes.h"
#include "fail.h"
#include "tupletriple.h"
#include "nots.h"

#ifndef STATIC_DEBUG_DISABLED
extern bool debug;
#endif

//L*(x, y) and R*(x, y) elements: store weight and extension node
struct weighted_extension {
	weight_t weight;
	nodeid_t node;
	weighted_extension(weight_t w, nodeid_t e) : weight(w), node(e) {}
	bool operator==(const weighted_extension &e) const { return weight == e.weight && node == e.node; }
	struct hash { std::size_t operator()(const weighted_extension &e) const noexcept {
		return e.node * e.weight;
	}};
};

typedef std::unordered_set<weighted_extension, weighted_extension::hash> ext_set;

#include "print.h"

struct apasp_data {

	struct updatenum_counts {
		struct unc {
			ui4 update_num;
			ui4 count;
			unc(ui4 un, ui4 ct) : update_num(un), count(ct) {}
		};
		std::vector<unc> uncs;

		void add(ui4 update_num, ui4 count) {
			for (ui4 i = 0; i < uncs.size(); i++)
				if (uncs[i].update_num == update_num) {
					uncs[i].count += count;
					return;
				}
			uncs.emplace_back(update_num, count);
		}
		void clear() {
			uncs.clear();
		}
	};

	struct cleanup_info {
		ui4 marked_count = 0;
		weight_t left_dist = 0, right_dist = 0;
		//ui4 max_update_num = 0;
		updatenum_counts unc;
	};

	//information kept for each triple
	struct bookkeeping_info {
		ui4 *paths_through, update_num = 0, pstar_update_num = 0;
		bookkeeping_info() : paths_through(nullptr) {}
	};

	typedef std::unordered_map<triple_abw, cleanup_info, triple_abw::hash> cleanup_matrix_elem;

	//One of these for each (x, y).P(x, y), P*(x, y), L*(x, y), R*(x, y) and maps b -> L(x, b y), a -> R(x a, y)
	struct matrix_elem {
		bool have_extracted = false;

		tripleset paths, shortest_paths; //P and P*
		ext_set left_star, right_star;
		std::unordered_map<nodeid_t, ext_set> left, right;
		
		std::unordered_map<triple_abw, bookkeeping_info, triple_abw::hash> info;
		cleanup_matrix_elem clinfo;

		weight_t edge_weight = 0, shortest_dist = 0;
	};

	matrix_elem *mtx;
	//cleanup_matrix_elem *cleanup_data;
	ui4 *vtx_update_num;
	ui4 nnodes;

	matrix_elem &mat(nodeid_t x, nodeid_t y) { return mtx[x * nnodes + y]; }
	cleanup_matrix_elem &clinfo(nodeid_t x, nodeid_t y) { return mtx[x * nnodes + y].clinfo; }
	//cleanup_matrix_elem &clinfo(nodeid_t x, nodeid_t y) { return cleanup_data[x * nnodes + y]; }

	void clear_cleanup_data() {
		for (ui4 i = 0; i < nnodes * nnodes; i++) {
			mtx[i].have_extracted = false;
			mtx[i].clinfo.clear();
		}
	}

	bool &have_extracted(nodeid_t x, nodeid_t y) { return mat(x, y).have_extracted; }

	ui4 &left_dist(const triple &t) { return clinfo(t.x, t.y)[t].left_dist; }
	ui4 &right_dist(const triple &t) { return clinfo(t.x, t.y)[t].right_dist; }

	updatenum_counts &history(const triple &t) { return clinfo(t.x, t.y)[t].unc; }
	ui4 &marked_count(const triple &t) { return clinfo(t.x, t.y)[t].marked_count; }
	ui4 &marked_count(nodeid_t x, nodeid_t a, nodeid_t b, nodeid_t y, weight_t wt) {
		return clinfo(x, y)[triple_abw(a, b, wt)].marked_count;
	}

	void mark_halves(triple &t) {
		nodeid_t temp = t.a;
		t.a = 1 + nnodes;
		marked_count(t) += t.count;
		t.a = temp;
		temp = t.b;
		t.b = 1 + nnodes;
		marked_count(t) += t.count;
		t.b = temp;
	}

	ui4 &update_num(const triple &t) {
		return mat(t.x, t.y).info[t].update_num;
	}
	ui4 &pstar_update_num(const triple &t) {
		return mat(t.x, t.y).info[t].pstar_update_num;
	}
	ui4 &pstar_update_num(nodeid_t x, nodeid_t y, const triple_abw &abw) {
		return mat(x, y).info[abw].pstar_update_num;
	}

	bool has_path_through(const triple &t, nodeid_t v) {
		if (t.x == v || t.a == v || t.b == v || t.y == v)
			return true;
		return paths(t, v) > 0;
	}
	ui4 &paths(const triple &t, nodeid_t v) {
		ui4 *&p = mat(t.x, t.y).info[t].paths_through;
		if (!p)
			p = new ui4[nnodes]();
		return p[v];
	}

	tripleset&    P(nodeid_t x, nodeid_t y)     { return mat(x, y).paths; }
	tripleset&    Pstar(nodeid_t x, nodeid_t y) { return mat(x, y).shortest_paths; }
	ext_set& Lstar(nodeid_t x, nodeid_t y) { return mat(x, y).left_star; }
	ext_set& Rstar(nodeid_t x, nodeid_t y) { return mat(x, y).right_star; }
	ext_set& L(nodeid_t x, nodeid_t b, nodeid_t y) {
		if (mat(x, y).left.count(b) == 0)
			mat(x, y).left[b] = ext_set();
		return mat(x, y).left[b];
	}
	ext_set& R(nodeid_t x, nodeid_t a, nodeid_t y) {
		if (mat(x, y).right.count(a) == 0)
			mat(x, y).right[a] = ext_set();
		return mat(x, y).right[a];
	}

	weight_t &edge_weight(nodeid_t x, nodeid_t y) { return mat(x, y).edge_weight; }
	weight_t &sp_dist(nodeid_t x, nodeid_t y) { return mat(x, y).shortest_dist; }

	bool inP(const triple &t)     { return P(t.x, t.y).contains(t); }
	bool inPstar(const triple &t) { return Pstar(t.x, t.y).contains(t); }

	void set_beta(const triple &t, char beta) {
		P(t.x, t.y).set_beta(t, beta);
	}

	//If t exists in P, add npaths to its count.  If not, add to to P and add x to L(a, b y) and y to R(x a, b).
	void add_triple(const triple &t, ui4 npaths) {
		if (P(t.x, t.y).add_triple(t, npaths)) {
			if (debug) printf("\t\t\tmodified count of %s by %d in P, count is now %u\n", t.cstr(), npaths, P(t.x, t.y).get_count(t));
			if (debug) std::cout <<"\t\t\t  L(" <<t.a <<", " <<t.b <<", " <<t.y <<") = " <<L(t.a, t.b, t.y) <<std::endl;
			if (debug) std::cout <<"\t\t\t  R(" <<t.x <<", " <<t.a <<", " <<t.b <<") = " <<R(t.x, t.a, t.b) <<std::endl;
		} else {
			if (debug) printf("\t\t\tadded %s to P(%d, %d)\n", t.cstr(), t.x, t.y);

			if (debug) printf("\t\t\tadd %d to L(%d, %d, %d)\n", t.x, t.a, t.b, t.y);
			L(t.a, t.b, t.y).emplace(t.weight, t.x);

			if (debug) printf("\t\t\tadd %d to R(%d, %d, %d)\n", t.y, t.x, t.a, t.b);
			R(t.x, t.a, t.b).emplace(t.weight, t.y);
		} 
	}
	//Add t to P*, <wt, x> to L*(a, y), and <wt, y> to R*(x, b)
	void add_shortest_triple(const triple &t, ui4 npaths) {
		if (debug) printf("\t\t\tadd_shortest_triple %s %d\n", t.cstr(), npaths);

		if (Pstar(t.x, t.y).add_triple(t, npaths)) {
			if (debug) printf("\t\t\tmodified count of %s by %d in P*\n", t.cstr(), npaths);
		} else {
			if (debug) printf("\t\t\t  added %s to P*(%u, %u)", t.cstr(), t.x, t.y);
			if (debug) printf("\t\t\t  add <%u, %u> to L*(%u, %u), add <%u, %u> to R*(%u, %u)\n", t.weight, t.x,    t.a, t.y, t.weight, t.y,   t.x, t.b);
			
			Lstar(t.a, t.y).emplace(t.weight, t.x);
			Rstar(t.x, t.b).emplace(t.weight, t.y);
		}
	}
	
	//Decrement t in P.  If it was completely removed, remove t.x from L and t.y from R and return true. 
	bool clean_triple(const triple &t, ext_set::iterator *lit, ext_set::iterator *rit, bool keep_LR=false) {
		if (debug) printf("\t\t\t  (clt) remove %s from P(%u, %u)\n", t.cstr(), t.x, t.y);
		P(t.x, t.y).decrement_triple(t);

		//if (!keep_LR && !P(t.x, t.y).has_triple_for_tuple(t.a, t.b)) {
		if (!keep_LR && !P(t.x, t.y).contains(t)) {
			if (debug)
				printf("\t\t\t  erase %u from L(%u, %u, %u), erase %u from R(%u, %u, %u)\n",
					   t.x,    t.a, t.b, t.y,        t.y,    t.x, t.a, t.b);

			ext_set &l = L(t.a, t.b, t.y);
			if (lit) {
				auto i = l.find({t.weight, t.x});
				if (i != l.end())
					*lit = l.erase(i);
			} else l.erase({t.weight, t.x});

			ext_set &r = R(t.x, t.a, t.b);
			if (rit) {
				auto i = r.find({t.weight, t.y});
				if (i != r.end())
					*rit = r.erase(i);
			} else r.erase({t.weight, t.y});

			if (debug) std::cout <<"\t\t\t\t--> L = " <<L(t.a, t.b, t.y) <<"\tR = " <<R(t.x, t.a, t.b) <<std::endl;
            return true;
		} else if (debug) std::cout <<"\t\t\t\t have triple for tuple?  P = " <<P(t.x, t.y).expanded(t.x, t.y) <<std::endl;
		return false;
	}
	//Decrement t in P*.  Then remove <wt, x> from L*(a, y) and <wt, y> from R*(x, b)
	void clean_shortest_triple(const triple &t) {
		if (debug) printf("\t\t\t (clst) remove %s from P*(%u, %u), remove <%u, %u> from L*(%u, %u), remove <%u, %u> from R*(%u, %u)\n",
						  t.cstr(), t.x, t.y,     t.weight, t.x,    t.a, t.y,     t.weight, t.y,     t.x, t.b);

		Pstar(t.x, t.y).decrement_triple(t);
		
		if (Pstar(t.a, t.y).empty())
			Lstar(t.a, t.y).erase({t.weight, t.x});

		if (Pstar(t.x, t.b).empty())
			Rstar(t.x, t.b).erase({t.weight, t.y});
	}

	apasp_data(ui4 nnodes) : nnodes(nnodes) {
		mtx = new matrix_elem[nnodes * nnodes]();
		//cleanup_data = new cleanup_matrix_elem[nnodes * nnodes]();
		vtx_update_num = new ui4[nnodes];
		for (ui4 i = 0; i <nnodes; ++i)
			vtx_update_num[i] = 0;
	}
};

